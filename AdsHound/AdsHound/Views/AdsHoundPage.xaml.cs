﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AdsHound.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdsHoundPage : ContentPage
    {
        public AdsHoundPage()
        {
            InitializeComponent();
        }
    }
}